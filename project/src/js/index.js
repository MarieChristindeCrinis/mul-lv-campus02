const searchPart = document.getElementById('search-part');
const detailPart = document.getElementById('pokemon-detail-part');

const pokemonName = document.getElementById('selected-pokemon-name');
const pokemonNr = document.getElementById('selected-pokemon-nr');
const pokemonImage = document.getElementById('selected-pokemon-image');
const pokemonStats = document.getElementById('selected-pokemon-stats');
const pokemonTypes = document.getElementById('selected-pokemon-types');
const btBack = document.getElementById('bt-back');
btBack.addEventListener('click', hideDetailView);

const searchText = document.getElementById("mySearch");
const list = document.getElementById("myPokémon");
searchText.addEventListener("keyup" , search);

const btUpload = document.getElementById('btFileUpload');
btUpload.addEventListener('input', readUpload);

document.addEventListener("DOMContentLoaded" , loadData);
var pokemons = [];

function search() 
{
    clearSelection();
    if(searchText.value === '' || searchText.value === undefined)
    {
        return;
    }

    var filteredPokes = pokemons.filter(x=> x.name.toLowerCase().includes(searchText.value.toLowerCase()));
    console.log(filteredPokes);

    for (const pokemon of filteredPokes) 
    {
        var pokEntry = document.createElement('li');
        var pokeName = document.createElement('span');
        pokeName.innerHTML = pokemon.name;
        pokeName.classList.add('pokemon-item');

        pokEntry.appendChild(pokeName);
        pokEntry.addEventListener('click', () => selectPokemon(pokemon.name))

        list.appendChild(pokEntry);
    }
}

function readUpload()
{
    console.dir(btUpload);
    const reader = new FileReader();
    reader.addEventListener('load', processfile);
    reader.readAsText(btUpload.files[0]);

    btUpload.value = '';
}

//https://developer.mozilla.org/en-US/docs/Web/XSLT/XSLT_JS_interface_in_Gecko/JavaScript_XSLT_Bindings
function processfile(fileContent) 
{
    console.log(fileContent.currentTarget.result);
    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(fileContent.currentTarget.result, "text/xml");
    console.log(xmlDoc);

    var xsltProcessor = new XSLTProcessor();
    var myXMLHTTPRequest = new XMLHttpRequest();
    myXMLHTTPRequest.open("GET", "assets/xml/pokemon.xsl", false);
    myXMLHTTPRequest.send(null);

    var xslRef = myXMLHTTPRequest.responseXML;
    console.log(xslRef);
    xsltProcessor.importStylesheet(xslRef);

    var docFrag = xsltProcessor.transformToDocument(xmlDoc);
    console.log(docFrag.body.innerHTML);

    var newWindow = window.open();
    newWindow.document.body.innerHTML = docFrag.body.innerHTML;
}

async function selectPokemon(name) 
{
    searchPart.style = "display: none";
    detailPart.style = "display: inline";

    pokemonName.innerHTML = name;

    var rawPokeDetail = await fetch("https://pokeapi.co/api/v2/pokemon/" + name);
    var pokeDetail = await rawPokeDetail.json();
    console.log(pokeDetail);

    pokemonImage.src = pokeDetail.sprites['other']['official-artwork'].front_default;
    pokemonNr.innerHTML = "Nr." + FormatPokemonNr(pokeDetail.id);

    for(const stat of pokeDetail.stats)
    {
        const statEntry = document.createElement("li");
        const statName = document.createElement("p");
        statName.classList.add('stat-part');
        statName.innerHTML = stat.stat.name;

        const statValue = document.createElement("p");
        statValue.classList.add('stat-part');
        statValue.innerHTML = stat.base_stat;

        statEntry.classList.add('two-column-layout');
        statEntry.appendChild(statName);
        statEntry.appendChild(statValue);

        pokemonStats.appendChild(statEntry);
    }

    for(const typ of pokeDetail.types)
    {
        const statEntry = document.createElement("li");
        statEntry.innerHTML = typ.type.name;
        statEntry.classList.add('background-color-' + typ.type.name);

        pokemonTypes.appendChild(statEntry);
    }
}

function FormatPokemonNr(nr)
{
    nr = nr.toString();
    while (nr.length < 3) nr = "0" + nr;
    return nr;
}

function hideDetailView()
{
    clearSelection();
    clearStats();
    clearTypes();

    searchText.value = '';
    searchPart.style = "display: inline";
    detailPart.style = "display: none";
    pokemonImage.scr = '';
}

function clearSelection()
{
    for (var i = 0; i < list.children.length;) 
    {
        list.removeChild(list.children[i]);
    }
}

function clearStats()
{
    for (var i = 0; i < pokemonStats.children.length;) 
    {
        pokemonStats.removeChild(pokemonStats.children[i]);
    }
}

function clearTypes()
{
    for (var i = 0; i < pokemonTypes.children.length;) 
    {
        pokemonTypes.removeChild(pokemonTypes.children[i]);
    }
}

async function loadData()
{
    try 
    {
        var data = await fetch("https://pokeapi.co/api/v2/pokemon/");
        var jsonObj = await data.json();
        var allData = await fetch("https://pokeapi.co/api/v2/pokemon/?limit=" + jsonObj.count);
        var allJsonObj = await allData.json();
        pokemons = allJsonObj.results;
    } 
    catch (error) 
    {
        console.log('Error in request: ', err);
    }

    detailPart.style = "display: none";
}