<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/sights">   
    <html>
        <head>

        </head>
        <body>
            <h1>Sight Results</h1>
            <xsl:for-each select="sigth">
                <h2>Location: <xsl:value-of select="place"/></h2>
                <ul>
                    <xsl:for-each select="pokemons">
                        <li><xsl:value-of select="pokemon"/> <xsl:value-of select="pokemon[@count]"/></li>
                    </xsl:for-each>
                </ul>
            </xsl:for-each>
        </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
