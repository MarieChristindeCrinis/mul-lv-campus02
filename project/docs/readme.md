# Pokejekt

## Einleitung
Pokejekt dient zum Abruf der Daten von Pokemon. Hierzu muss der Name des Pokemons im Suchfeld ausgewählt werden und im Anschluss auf das gewünschte Pokemon geklickt werden um so Informationen zu diesem Pokemon betrachten zu können.

Weiters können auch Touren in XML hochgeladen werden um diese Touren dann in einer HTML Seite darzustellen.

## Anforderungsanalyse

Benutzer sollen den Namen eines Pokemon eingeben können, darauf klicken können und dann die Stats diese Pokemons sehen.

Benutzer sollen eine Tour als XML hochladen können um diese zu betrachten.

## Systemarchitektur

Das System sieht folgendermaßen aus:

![alt text](./architektur.png "Architektur Bild")

## Datenmodel

### XML-Dokument

Das XML-Dokument enthält als Root Element ein `sights` dieses enthält mehrere `sight` Elemente. In einer befindet sich immer ein `place` und mehrere `pokemon` welche sich innerhalb eines `pokemons` Element befinden. Ein `pokemon` Element enthält zusätzlich noch ein `count` Attribut über welches angegeben wird wie viele dieser Pokemon man begegnet ist. 

Ein Beispiel XML Dokument:

```
<?xml version="1.0" encoding="UTF-8"?>
<sights xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="./sightValidation.xsd">
    <sigth>
        <pokemons>
            <pokemon count='1'>ditto</pokemon>
        </pokemons>
        <place>Lake of Outrage</place>
    </sigth>
</sights>
```
## Schnittstellenbeschreibung

Als Schnittstelle wird PokéAPi: (https://pokeapi.co/api/v2/pokemon/) verwendet. Diese API bietet nicht nur Zugriff auf Pokémon, sondern auch auf große Teile des kompletten Pokémon-Universums, seien es Items, Orte oder Attacken der Pokémons. Bei der Route steht "v2" für die Version 2 und "pokemon" beinhaltet alle bekannten Pokémon. Weiters kann bei der /pokemon Abfrage noch Parameter wie limit und offset hinzugefügt werden. Diese Parameter werden für die Pagination benötigt. Zudem wird die gesamte Anzahl an bekannten Pokémon sowie die Query für nächste sowie vorhergehende Seite übermittelt. Als HTTP Methode wird die GET-Request verwendet, indem die Fetch-Methode intern eine GET-Request durchführt.



## Projektablauf

Um das Projekt umzusetzen, wurde als IDE Visual Studio Code verwendet. Die gemeinsame Projektarbeit wurde via GitLab und Discord-Meetings durchgeführt. Eine Taskaufteilung per se gab es daher nicht, da gemeinsam an der Umsetzung und Fertigstellung des Projekts gearbeitet wurde. 


