
//https://developer.deutschebahn.com/store/apis/info?name=Fahrplan-Free&version=v1&provider=DBOpenData
/*Quellen:
https://developer.mozilla.org/de/docs/Web/API/URLSearchParams, https://wiki.selfhtml.org/wiki/JavaScript/Schleife,https://developer.deutschebahn.com/store/apis/info?name=Fahrplan-Free&version=v1&provider=DBOpenData#!/default/get_arrivalBoard_id, https://www.w3schools.com/jsref/dom_obj_search.asp, https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Date, https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Text_formatting, https://www.w3schools.com/bootstrap/bootstrap_ref_all_classes.asp
*/
var searchText = document.getElementById("mySearch");
var list = document.getElementById("myBahnhof");
var currentList = null;
var arrival = document.getElementById("myAnkunft");
var table = document.getElementById("myTable");



function search(){

  for(var i = 0; i < list.children.length;){
    list.removeChild(list.children[i]);
  }
  console.dir(searchText);

  fetch("http://api.deutschebahn.com/freeplan/v1/location/"+searchText.value)
  .then(response => response.json())
  .then(json => {
    console.log(json);
    currentList = json;
    for (var i = 0; i < json.length; i++){
      var element = document.createElement("li");
      element.innerHTML = json[i].name;
      element.classList.add("list-group-item");
      list.appendChild(element);
    }
  })
  .catch(err => {
    console.log('Error in request: ',err);
  });

}

function arrivalInfo(event){
  var id = null;
  for(var i = 0; i < currentList.length; i++){
    if(currentList[i].name === event.target.innerHTML){
      id = currentList[i].id;
      i=currentList.length;
    }
  }
  console.log(id);
  var date = new Date(Date.now());
  var monthString = ""+date.getMonth();
  var dayString = ""+date.getDay();
  var hourString = ""+date.getHours();
  var minuteString = ""+date.getMinutes();
  var secondString = ""+date.getSeconds();
  if(monthString.length !== 2)
  {
    monthString = "0"+monthString;
  }
  if(dayString.length !== 2)
  {
    dayString = "0"+dayString;
  }
  if(minuteString.length !== 2){
    minuteString = "0"+minuteString;
  }
  if(secondString.length !== 2){
    secondString = "0"+secondString;
  }
  if(hourString.length !== 2){
    hourString = "0"+hourString;
  }

  fetch("https://api.deutschebahn.com/freeplan/v1/arrivalBoard/"+id+"?date="+`${date.getFullYear()}-${monthString}-${dayString}T${hourString}:${minuteString}:${secondString}`)
  .then( response => response.json())
  .then(json => {
    console.log(json);
    for(var i = 0; i < json.length; i++){
      var row = table.insertRow(1);
      var name = row.insertCell(0);
      name.innerHTML = json[i].name;
      var datum = row.insertCell(1);
      datum.innerHTML = json[i].dateTime;
      var ziel = row.insertCell(2);
      ziel.innerHTML = json[i].origin;
      var bahnsteig = row.insertCell(3);
      bahnsteig.innerHTML = json[i].track;

    }
    

  });
}
