<?xml version="1.0" encoding="UTF-8"?>
<!-- Quellen: https://gitlab.com/a.steinmaurer/mul-lv-campus02/-/blob/master/lecture/06-code/02-liste.xsl; https://www.w3schools.com/html/html_emojis.asp; https://www.w3schools.com/xml/xsl_choose.asp; https://www.w3schools.com/css/css_howto.asp  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/dictionary">   

    <html>
        <head>
            <style>
                body{
                    background-color: #f0fff0;
                    
                }
                .content{
                    max-width: 500px;
                    margin: auto;
                    
                    
                    
                }
                h2{
                    color: black;
                    text-align: center;
                    font-style: italic;
                    
                }
                .center-vert {
                    margin-left: auto;
                    margin-right: auto;
                    font-style: normal;
                    
                }

            </style>
          </head>    
        <body>
            <div class="content">
            <h2 style="vertical-align: middle">Wörterliste   &#128221;</h2>
            <table class="center-vert">
                <tr>
                    <th>Englisch</th>
                    <th>Deutsch</th>
                    <th>Kategorie</th>
                </tr>
                <xsl:for-each select="word">
                  <xsl:sort select="@value" order="descending"/>
                <tr>
                    <td><xsl:value-of select="@value"/></td> 
                    <td><xsl:value-of select="translation[@lang = 'DE']"/></td>
                    <td>
                        <xsl:choose>
                        <xsl:when test="category='Animal'">
                            &#128008;
                        </xsl:when>
                        <xsl:when test="category='Geography'">
                            &#128507;
                        </xsl:when>
                        <xsl:when test="category='Food'">
                           &#127843;
                        </xsl:when>
                      </xsl:choose>
                      <xsl:value-of select="category"/>
                    </td>
                </tr>

                </xsl:for-each>
            </table>
            <h2 style="vertical-align: middle">Statistik  &#128200; </h2>
            <table class="center-vert">
                <tr>
                    <td>
                    Die Liste enthält ingesamt <xsl:value-of select="count(word)"/> Vokabeln
                    </td> 
                </tr>
                <tr>
                <td>
                    Deutsch: <xsl:value-of select="count(word/translation[@lang = 'DE'])"/>
                </td>
                </tr>
                <tr>
                <td>
                    Französisch: <xsl:value-of select="count(word/translation[@lang = 'FR'])"/>
                </td>
                <tr>
                <td>
                    Latein: <xsl:value-of select="count(word/translation[@lang = 'LA'])"/>
                </td>
                </tr>
                </tr>
            </table>
        </div>
    </body>
      </html>

  </xsl:template>
</xsl:stylesheet>
