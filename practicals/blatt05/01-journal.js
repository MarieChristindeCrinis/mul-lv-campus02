//Quellen: https://www.w3schools.com/; https://developer.mozilla.org/;

//console.log(document);

var journal = document.getElementById('journal');
//console.log(journal);
var titel = document.getElementById('titel');
//console.log(titel);
var eintrag = document.getElementById('eintrag');
//console.log(eintrag);
var posten = document.getElementById('fragen');
var counter = document.querySelector('span[class=counter]');
var selected = null;
var firstWebChild = null;
//console.log(posten);
posten.addEventListener('click', addEintrag);

var allTitel = document.querySelectorAll('div[class="titel"]');
//console.log(allTitel);
[...allTitel].forEach(function(each){
    each.addEventListener('dblclick', selectEintrag)
})

eintrag.addEventListener('keyup', function(e) {
    updateCounter();
});

var xhr = new XMLHttpRequest();

xhr.open("GET", "https://xn--4ca9a.eu/FH/posts.xml")
xhr.responseType = "document";
xhr.send();

xhr.onload = function () {

    console.log(xhr);

    if(xhr.status !== 200) {
        alert("Error while executing request to: https://xn--4ca9a.eu/FH/posts.xml");
    }
    else{
        console.log(xhr.response);
        const posts = xhr.response.getElementsByTagName("post");
        let postObj = new Array();
        for(var i = 0; i < posts.length; i++) {
            postObj.push({
                title: posts[i].getElementsByTagName("title")[0].innerHTML,
                content: posts[i].getElementsByTagName("content")[0].innerHTML,
                datum: posts[i].getElementsByTagName("datum")[0].innerHTML 
            });
        }
        console.log(postObj);
        postObj.sort(function(x,y) {
            var help = new Date(x.datum.split(".")[2], x.datum.split(".")[1], x.datum.split(".")[0],0,0,0,0) - new Date(y.datum.split(".")[2], y.datum.split(".")[1], y.datum.split(".")[0],0,0,0,0);
            console.log(help);
            return help;
        });
        console.log(postObj);
        for(var i = 0; i < postObj.length; i++) {
            x = postObj[i];
            var jEintrag = document.createElement('div');
            jEintrag.className='j-eintrag';
            var jTitel = document.createElement('div');
            jTitel.innerText = '[WEB:]' + x.title;
            jTitel.className = 'titel';
            jTitel.addEventListener('dblclick', selectEintrag);
            jEintrag.appendChild(jTitel);
            var jBeschreibung = document.createElement('div');
            jBeschreibung.innerText = x.content;
            jBeschreibung.className = 'eintrag';
            jEintrag.appendChild(jBeschreibung);
            var jDate = document.createElement('div');
            jDate.innerText = x.datum;
            jDate.className = 'datum';
            jEintrag.appendChild(jDate);
            var trennlinie = document.createElement('hr');
            jEintrag.appendChild(trennlinie);
            journal.appendChild(jEintrag);
            if(i == 0) {
                firstWebChild = jEintrag;
            }
        }
    }
}



function addEintrag(event){
    //console.log(event);
    if(titel.value != '' && eintrag.value != '') {
        var jEintrag = document.createElement('div');
        jEintrag.className='j-eintrag';
        var jTitel = document.createElement('div');
        jTitel.innerText = titel.value;
        jTitel.className = 'titel';
        jTitel.addEventListener('dblclick', selectEintrag);
        jEintrag.appendChild(jTitel);
        var jBeschreibung = document.createElement('div');
        jBeschreibung.innerText = eintrag.value;
        jBeschreibung.className = 'eintrag';
        jEintrag.appendChild(jBeschreibung);
        var trennlinie = document.createElement('hr');
        jEintrag.appendChild(trennlinie);
        journal.insertBefore(jEintrag, firstWebChild);
    }
    titel.value = '';
    eintrag.value = '';
    updateCounter();
}

function selectEintrag(event){
    //console.log(event);
    selected = event.target.parentNode;
    //console.dir(selected);
    [...selected.children].forEach(function(each){
       if(each.className == 'titel') {
           titel.value = each.innerText;
       }
       else if(each.className == 'eintrag') {
           eintrag.value = each.innerText;
       }
    });
    updateCounter();
    posten.value = 'Update!';
    posten.removeEventListener('click', addEintrag);
    posten.addEventListener('click', updateEintrag)
}

function updateEintrag(event){
    [...selected.children].forEach(function(each){
        if(each.className == 'titel') {
            each.innerText = titel.value;
        }
        else if(each.className == 'eintrag') {
            each.innerText = eintrag.value;
        }
     });
     titel.value = '';
     eintrag.value = '';
     updateCounter();
     posten.value='Posten!';
     selected = null;
     posten.addEventListener('click', addEintrag);
     posten.removeEventListener('click', updateEintrag);     
}

function updateCounter(){
    var eintragText = eintrag.value;
    counter.innerText = eintragText.length;
}