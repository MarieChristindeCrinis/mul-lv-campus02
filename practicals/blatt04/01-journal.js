//Quellen: https://www.w3schools.com/; https://developer.mozilla.org/; https://gitlab.com/a.steinmaurer/mul-lv-campus02/-/tree/master

//console.log(document);

var journal = document.getElementById('journal');
//console.log(journal);
var titel = document.getElementById('titel');
//console.log(titel);
var eintrag = document.getElementById('eintrag');
//console.log(eintrag);
var posten = document.getElementById('fragen');
var counter = document.querySelector('span[class=counter]');
var selected = null;
//console.log(posten);
posten.addEventListener('click', addEintrag);

var allTitel = document.querySelectorAll('div[class="titel"]');
//console.log(allTitel);
[...allTitel].forEach(function(each){
    each.addEventListener('dblclick', selectEintrag)
})

eintrag.addEventListener('keyup', function(e) {
    updateCounter();
});

function addEintrag(event){
    //console.log(event);
    if(titel.value != '' && eintrag.value != '') {
        var jEintrag = document.createElement('div');
        jEintrag.className='j-eintrag';
        var jTitel = document.createElement('div');
        jTitel.innerText = titel.value;
        jTitel.className = 'titel';
        jTitel.addEventListener('dblclick', selectEintrag);
        jEintrag.appendChild(jTitel);
        var jBeschreibung = document.createElement('div');
        jBeschreibung.innerText = eintrag.value;
        jBeschreibung.className = 'eintrag';
        jEintrag.appendChild(jBeschreibung);
        var trennlinie = document.createElement('hr');
        jEintrag.appendChild(trennlinie);
        journal.appendChild(jEintrag);
    }
    titel.value = '';
    eintrag.value = '';
    updateCounter();
}

function selectEintrag(event){
    //console.log(event);
    selected = event.target.parentNode;
    //console.dir(selected);
    [...selected.children].forEach(function(each){
       if(each.className == 'titel') {
           titel.value = each.innerText;
       }
       else if(each.className == 'eintrag') {
           eintrag.value = each.innerText;
       }
    });
    updateCounter();
    posten.value = 'Update!';
    posten.removeEventListener('click', addEintrag);
    posten.addEventListener('click', updateEintrag)
}

function updateEintrag(event){
    [...selected.children].forEach(function(each){
        if(each.className == 'titel') {
            each.innerText = titel.value;
        }
        else if(each.className == 'eintrag') {
            each.innerText = eintrag.value;
        }
     });
     titel.value = '';
     eintrag.value = '';
     updateCounter();
     posten.value='Posten!';
     selected = null;
     posten.addEventListener('click', addEintrag);
     posten.removeEventListener('click', updateEintrag);     
}

function updateCounter(){
    var eintragText = eintrag.value;
    counter.innerText = eintragText.length;
}