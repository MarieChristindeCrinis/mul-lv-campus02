<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match= "/gameCollection">
<html>
    <body>
        <h2>My games</h2>
        <table border = "1">
            <tr bgcolor = "lightblue">
                <th>Title</th>
                <th>Hours played</th>
                <th>Part</th>
            </tr>
            <xsl:for-each select = "publisher">
                <xsl:for-each select = "game">
                <tr>
                    <td><xsl:value-of select = "title"/></td>
                    <td><xsl:value-of select = "gameTime"/></td>
                    <td><xsl:value-of select = "part"/></td>
                </tr>
            </xsl:for-each>
            </xsl:for-each>
        </table>
    </body>
</html>

</xsl:template>
</xsl:stylesheet>
